using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WebPageSearch
{
    public partial class WebPageSearch : Form
    {
        private TabControl browserTab;
        private TabPage browserPage;
        private WebBrowser webBrowser;
        private TextBox urlTextBox;
        private TabPage searchPage;
        private Button loadPageButton;
        private Label positionLabel;
        private TabPage historyTab;
        private ListBox historyListBox;
        private Button searchButton;
        private TextBox searchTextBox;
        private CheckBox matchCaseCheckBox;
        private Label statusLabel;
        private RichTextBox richTextBox;
        private int _startSearchPosition = 0;
      
        public WebPageSearch()
        {
            InitializeComponent();
        }
      
        private void exitButton_Click(object sender, EventArgs e)
        {
            Close();
        }
      
        private void loadPageButton_Click(object sender, EventArgs e)
        {
            if (urlTextBox.Text.Length > 0)
            {
                webBrowser.Navigate(urlTextBox.Text);
            }
        }
      
        private void webBrowser_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            statusLabel.Text = "Loading...";
        }
      
        private void webBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            statusLabel.Text = "Loading web page source...";
            richTextBox.Text = webBrowser.DocumentText.ToString();
            statusLabel.Text = "Idle";
            Cursor = Cursors.Default;
        }
      
        private void searchButton_Click(object sender, EventArgs e)
        {
            string searchString = searchTextBox.Text;
            int position = -1;
            
            if (searchString.Length > 0)
            {
                statusLabel.Text = "Searching...";
                if (matchCaseCheckBox.Checked)
                {
                    position = richTextBox.Find(searchString, _startSearchPosition, RichTextBoxFinds.MatchCase);
                }
                else
                {
                    position = richTextBox.Find(searchString, _startSearchPosition, RichTextBoxFinds.None);
                }
                if (position >= 0)
                {
                    _startSearchPosition = position + 1;
                    positionLabel.Text = position.ToString();
                    richTextBox.SelectionColor = Color.Red;
                    richTextBox.Select(position, searchString.Length);
                    richTextBox.ScrollToCaret();
                }
                else
                {
                    positionLabel.Text = "Not found";
                    _startSearchPosition = 0;
                }
                statusLabel.Text = "Idle";
            }
        }

        private void InitializeComponent()
        {
            this.browserTab = new System.Windows.Forms.TabControl();
            this.browserPage = new System.Windows.Forms.TabPage();
            this.loadPageButton = new System.Windows.Forms.Button();
            this.webBrowser = new System.Windows.Forms.WebBrowser();
            this.urlTextBox = new System.Windows.Forms.TextBox();
            this.searchPage = new System.Windows.Forms.TabPage();
            this.statusLabel = new System.Windows.Forms.Label();
            this.matchCaseCheckBox = new System.Windows.Forms.CheckBox();
            this.searchButton = new System.Windows.Forms.Button();
            this.searchTextBox = new System.Windows.Forms.TextBox();
            this.positionLabel = new System.Windows.Forms.Label();
            this.historyTab = new System.Windows.Forms.TabPage();
            this.historyListBox = new System.Windows.Forms.ListBox();
            this.richTextBox = new System.Windows.Forms.RichTextBox();
            this.browserTab.SuspendLayout();
            this.browserPage.SuspendLayout();
            this.searchPage.SuspendLayout();
            this.historyTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // browserTab
            // 
            this.browserTab.Controls.Add(this.browserPage);
            this.browserTab.Controls.Add(this.searchPage);
            this.browserTab.Controls.Add(this.historyTab);
            this.browserTab.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.browserTab.Location = new System.Drawing.Point(24, 12);
            this.browserTab.Name = "browserTab";
            this.browserTab.SelectedIndex = 0;
            this.browserTab.Size = new System.Drawing.Size(720, 458);
            this.browserTab.TabIndex = 0;
            // 
            // browserPage
            // 
            this.browserPage.Controls.Add(this.loadPageButton);
            this.browserPage.Controls.Add(this.webBrowser);
            this.browserPage.Controls.Add(this.urlTextBox);
            this.browserPage.Location = new System.Drawing.Point(4, 24);
            this.browserPage.Name = "browserPage";
            this.browserPage.Padding = new System.Windows.Forms.Padding(3);
            this.browserPage.Size = new System.Drawing.Size(712, 430);
            this.browserPage.TabIndex = 2;
            this.browserPage.Text = "Web Browser";
            this.browserPage.UseVisualStyleBackColor = true;
            // 
            // loadPageButton
            // 
            this.loadPageButton.Location = new System.Drawing.Point(637, 11);
            this.loadPageButton.Name = "loadPageButton";
            this.loadPageButton.Size = new System.Drawing.Size(69, 23);
            this.loadPageButton.TabIndex = 8;
            this.loadPageButton.Text = "Go";
            this.loadPageButton.UseVisualStyleBackColor = true;
            this.loadPageButton.Click += new System.EventHandler(this.loadPageButton_Click_1);
            // 
            // webBrowser
            // 
            this.webBrowser.Location = new System.Drawing.Point(51, 58);
            this.webBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser.Name = "webBrowser";
            this.webBrowser.Size = new System.Drawing.Size(586, 339);
            this.webBrowser.TabIndex = 1;
            // 
            // urlTextBox
            // 
            this.urlTextBox.Location = new System.Drawing.Point(59, 13);
            this.urlTextBox.Name = "urlTextBox";
            this.urlTextBox.Size = new System.Drawing.Size(567, 23);
            this.urlTextBox.TabIndex = 0;
            // 
            // searchPage
            // 
            this.searchPage.Controls.Add(this.richTextBox);
            this.searchPage.Controls.Add(this.statusLabel);
            this.searchPage.Controls.Add(this.matchCaseCheckBox);
            this.searchPage.Controls.Add(this.searchButton);
            this.searchPage.Controls.Add(this.searchTextBox);
            this.searchPage.Controls.Add(this.positionLabel);
            this.searchPage.Location = new System.Drawing.Point(4, 24);
            this.searchPage.Name = "searchPage";
            this.searchPage.Padding = new System.Windows.Forms.Padding(3);
            this.searchPage.Size = new System.Drawing.Size(712, 430);
            this.searchPage.TabIndex = 3;
            this.searchPage.Text = "Web Page Source";
            this.searchPage.UseVisualStyleBackColor = true;
            // 
            // statusLabel
            // 
            this.statusLabel.AutoSize = true;
            this.statusLabel.Location = new System.Drawing.Point(241, 60);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(70, 15);
            this.statusLabel.TabIndex = 13;
            this.statusLabel.Text = "Status Label";
            // 
            // matchCaseCheckBox
            // 
            this.matchCaseCheckBox.AutoSize = true;
            this.matchCaseCheckBox.Location = new System.Drawing.Point(261, 23);
            this.matchCaseCheckBox.Name = "matchCaseCheckBox";
            this.matchCaseCheckBox.Size = new System.Drawing.Size(88, 19);
            this.matchCaseCheckBox.TabIndex = 10;
            this.matchCaseCheckBox.Text = "Match Case";
            this.matchCaseCheckBox.UseVisualStyleBackColor = true;
            // 
            // searchButton
            // 
            this.searchButton.Location = new System.Drawing.Point(355, 15);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(52, 29);
            this.searchButton.TabIndex = 9;
            this.searchButton.Text = "Search";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click_2);
            // 
            // searchTextBox
            // 
            this.searchTextBox.Location = new System.Drawing.Point(109, 21);
            this.searchTextBox.Name = "searchTextBox";
            this.searchTextBox.Size = new System.Drawing.Size(146, 23);
            this.searchTextBox.TabIndex = 8;
            // 
            // positionLabel
            // 
            this.positionLabel.AutoSize = true;
            this.positionLabel.Location = new System.Drawing.Point(325, 60);
            this.positionLabel.Name = "positionLabel";
            this.positionLabel.Size = new System.Drawing.Size(82, 15);
            this.positionLabel.TabIndex = 5;
            this.positionLabel.Text = "Search Results";
            // 
            // historyTab
            // 
            this.historyTab.Controls.Add(this.historyListBox);
            this.historyTab.Location = new System.Drawing.Point(4, 24);
            this.historyTab.Name = "historyTab";
            this.historyTab.Padding = new System.Windows.Forms.Padding(3);
            this.historyTab.Size = new System.Drawing.Size(712, 430);
            this.historyTab.TabIndex = 4;
            this.historyTab.Text = "History";
            this.historyTab.UseVisualStyleBackColor = true;
            // 
            // historyListBox
            // 
            this.historyListBox.FormattingEnabled = true;
            this.historyListBox.ItemHeight = 15;
            this.historyListBox.Location = new System.Drawing.Point(35, 20);
            this.historyListBox.Name = "historyListBox";
            this.historyListBox.Size = new System.Drawing.Size(652, 394);
            this.historyListBox.TabIndex = 0;
            // 
            // richTextBox
            // 
            this.richTextBox.Location = new System.Drawing.Point(22, 78);
            this.richTextBox.Name = "richTextBox";
            this.richTextBox.Size = new System.Drawing.Size(684, 333);
            this.richTextBox.TabIndex = 14;
            this.richTextBox.Text = "";
            // 
            // WebPageSearch
            // 
            this.ClientSize = new System.Drawing.Size(765, 483);
            this.Controls.Add(this.browserTab);
            this.Name = "WebPageSearch";
            this.Text = "Web Browser";
            this.browserTab.ResumeLayout(false);
            this.browserPage.ResumeLayout(false);
            this.browserPage.PerformLayout();
            this.searchPage.ResumeLayout(false);
            this.searchPage.PerformLayout();
            this.historyTab.ResumeLayout(false);
            this.ResumeLayout(false);

        }

     

        private void loadPageButton_Click_1(object sender, EventArgs e)
        {
            if (urlTextBox.Text.Length > 0)
            {
                webBrowser.Navigate(urlTextBox.Text);
                historyListBox.Items.Add(urlTextBox.Text);
                richTextBox.Text = webBrowser.DocumentText;
            }
        }

  

        private void searchButton_Click_2(object sender, EventArgs e)
        {
            string searchString = searchTextBox.Text;
            int position = -1;

            if (searchString.Length > 0)
            {
                statusLabel.Text = "Searching...";
                if (matchCaseCheckBox.Checked)
                {
                    position = richTextBox.Find(searchString, _startSearchPosition, RichTextBoxFinds.MatchCase);
                }
                else
                {
                    position = richTextBox.Find(searchString, _startSearchPosition, RichTextBoxFinds.None);
                }
                if (position >= 0)
                {
                    _startSearchPosition = position + 1;
                    positionLabel.Text = position.ToString();
                    richTextBox.SelectionColor = Color.Red;
                    richTextBox.Select(position, searchString.Length);
                    richTextBox.ScrollToCaret();
                }
                else
                {
                    positionLabel.Text = "Not found";
                    _startSearchPosition = 0;
                }
                statusLabel.Text = "Idle";
            }
        }


    }
}


